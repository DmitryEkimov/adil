﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adil.Dal.Models
{
    public class RequestsStatusesLink
    {
        public int Id { get; set; }
        public int RequestId { get; set; } // Номер статуса
        public int RPCode { get; set; } // Номер статуса
        public int StatusId { get; set; } // Номер статуса
        // Reverse navigation
        public virtual Request Request { get; set; } 
        public virtual Status Status { get; set; } 
    }
    internal class RequestsStatusesLinkConfiguration : EntityTypeConfiguration<RequestsStatusesLink>
    {
        public RequestsStatusesLinkConfiguration()
        {
            //ToTable("dbo.RequestStatusesLink");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RequestId).HasColumnName("RequestId").IsRequired();
            Property(x => x.RPCode).HasColumnName("RPCode").IsRequired();
            Property(x => x.StatusId).HasColumnName("StatusId").IsRequired();
            //HasRequired(a => a.Request).WithMany(b => b.RequestsStatusesLinks).HasForeignKey(c => new {c.RequestId, c.RPCode} );
            HasRequired(a => a.Status).WithMany(b => b.RequestsStatusesLinks).HasForeignKey(c => c.StatusId); 
        }
    }
}
