﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adil.Dal.Models
{
    public class RequestVM
    {
        [DisplayName("Номер заявки")]
        public int Id { get; set; } //Номер заявки
        [Required]
        [DisplayName("Код РП")]
        public int RPCode { get; set; } // Код РП
        [UIHint("RPStatusDropDown")]
        [Required]
        [DisplayName("Статус РП")]
        public int RPStatus { get; set; } // Статус РП
        [Required]
        //[MaxLength(256)]
        [DisplayName("ФИО")]
        public string FamilyName { get; set; } // ФИО
        [DisplayName("Фото")]
        [UIHint("FileUpload")]
        public byte[] Photo { get; set; }
        [UIHint("StatusIdDropDown")]
        [Required]
        [DisplayName("Статус ЦУ")]
        public int StatusId { get; set; } // Статус ЦУ
    }
}
