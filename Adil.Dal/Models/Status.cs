﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adil.Dal.Models
{
    public class Status
    {
        public int Id { get; set; } // Номер статуса
        public string StatusName { get; set; } // Наименование
        // Reverse navigation
        public virtual ICollection<RequestsStatusesLink> RequestsStatusesLinks { get; set; } 
    }
    internal class StatusConfiguration: EntityTypeConfiguration<Status>
    {
        public StatusConfiguration()
        {
            //ToTable("dbo.Status");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.StatusName).HasColumnName("StatusName").IsOptional().HasMaxLength(32);
        }
    }
}
