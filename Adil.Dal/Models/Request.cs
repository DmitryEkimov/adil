﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Adil.Dal.Models
{
    public class Request
    {
        public int Id { get; set; } //Номер заявки
        public int RPCode { get; set; } // Код РП
        public int RPStatus { get; set; } // Статус РП
        public string FamilyName { get; set; } // ФИО
        public byte[] Photo { get; set; }
        // Reverse navigation
        public virtual ICollection<RequestsStatusesLink> RequestsStatusesLinks { get; set; } 
    }
    internal class RequestConfiguration : EntityTypeConfiguration<Request>
    {
        public RequestConfiguration()
        {
            //ToTable("dbo.Requests");
            HasKey(x => new { x.Id,x.RPCode } );
            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RPCode).HasColumnName("RPCode").IsRequired();
            Property(x => x.RPStatus).HasColumnName("RPStatus").IsRequired();
            Property(x => x.FamilyName).HasColumnName("FamilyName").IsOptional().HasMaxLength(256);
            Property(x => x.Photo).HasColumnName("Photo").IsOptional();
        }
    }
}
