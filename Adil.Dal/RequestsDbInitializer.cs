﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Adil.Dal
{
    public class RequestsDbInitializer : IDatabaseInitializer<RequestsDbContext> 
    {

        public void InitializeDatabase(RequestsDbContext context)
        {
            var needtochange = false;
            bool dbExists = context.Database.Exists();
            
            if (dbExists)
            {
                var cmd = context.Database.Connection.CreateCommand();
                context.Database.Connection.Open();
                cmd.CommandText = @"select count(*) from sysobjects where xtype = 'U'";
                var tablecount = (int)cmd.ExecuteScalar();
                context.Database.Connection.Close();
                var needToCreateDB = ConfigurationManager.AppSettings["NeedToRecreateDB"].ToLower();
                if (tablecount == 0 || needToCreateDB == "true")
                {
                    needtochange = true;
                    //var configuration = new Migrations.Configuration();
                    //var migrator = new DbMigrator(configuration);
                    //migrator.Update();
                }
                //try
                //{
                //    needtochange = !context.Database.CompatibleWithModel(true);
                //}
                //catch (Exception ex)
                //{
                //    needtochange = true;
                //}
            }
            else
            {
                context.Database.Create();
                needtochange = true;
            }
        }
    }
}
