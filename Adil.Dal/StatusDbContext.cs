﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adil.Dal.Helpers;
using Adil.Dal.Models;
using System.Configuration;

namespace Adil.Dal
{
    public class StatusDbContext: DbContext
    {
        public IDbSet<Request> Requests { get; set; } // acts
        public IDbSet<RequestsStatusesLink> RequestsStatusesLinks { get; set; } // addresses
        public IDbSet<Status> Statuses { get; set; } // authors
        
        static StatusDbContext()
        {
            DbContextUtils<StatusDbContext>.SetInitializer(null);
        }

        public StatusDbContext()
            : base("Name=SecondConnection")
        {
        }
        public StatusDbContext(string connectionString)
            : base(connectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new StatusConfiguration());
            modelBuilder.Configurations.Add(new RequestsStatusesLinkConfiguration());
        }
    }
}
