﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adil.Dal.Models;

namespace Adil.Dal.Repository
{
    public class StatusesRepository : IStatusesRepository
    {
        private readonly StatusDbContext context;
        public StatusesRepository(StatusDbContext _context)
        {
            context = _context;
        }

        public IQueryable<Models.Status> GetAll()
        {
            var allq = from s in context.Statuses select s;
            if (!allq.Any())
            {
                context.Statuses.Add(new Status() { Id = 1, StatusName =  "Открыт" });
                context.Statuses.Add(new Status() { Id = 2, StatusName =  "Закрыт" });
                context.Statuses.Add(new Status() { Id = 95, StatusName = "Аннулировано" });
                context.SaveChanges();
                allq = context.Statuses;
            }
            return allq;
        }

        public string CodeToStatus(int id)
        {
            return context.Statuses.Find(id).StatusName;
        }

        public int StatusToCode(string status)
        {
            var stat = context.Statuses.FirstOrDefault(s => s.StatusName == status);
            return stat == null ? 0 : stat.Id;
        }
    }
}
