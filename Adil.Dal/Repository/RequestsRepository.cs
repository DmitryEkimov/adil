﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adil.Dal.Models;

namespace Adil.Dal.Repository
{
    public class RequestsRepository : IRequestsRepository
    {
        private readonly RequestsDbContext context;
        private readonly StatusDbContext statusdb;

        public RequestsRepository(RequestsDbContext _context, StatusDbContext _statusdb)
        {
            context = _context;
            statusdb = _statusdb;
        }
        public IQueryable<Models.RequestVM> GetAll()
        {
            var all = from r in context.Requests 
                select new
                    RequestVM()
                       {
                           FamilyName = r.FamilyName,
                           Id = r.Id,
                           RPCode = r.RPCode,
                           RPStatus = r.RPStatus,
                           StatusId = 0
                       };
            return all.AsQueryable();
        }

        public int GetStatus(int Id, int RPCode)
        {
            var l = statusdb.RequestsStatusesLinks.FirstOrDefault(u => u.RequestId == Id && u.RPCode == RPCode);
            return l == null ? 0 : l.StatusId;
        }

        public string GetStatusName(int Id, int RPCode)
        {
            var l = statusdb.RequestsStatusesLinks.Include("Status").FirstOrDefault(u => u.RequestId == Id && u.RPCode == RPCode);
            return l == null ? "" : l.Status.StatusName;
        }

        public Models.RequestVM GetSingle(int Id)
        {
            var r = context.Requests.FirstOrDefault(s=>s.Id==Id);
            if(r==null) return null;
            var l = statusdb.RequestsStatusesLinks.FirstOrDefault(u => u.RequestId == Id && u.RPCode == r.RPCode);
            var Model = 
                    new RequestVM()
                    {
                        FamilyName = r.FamilyName,
                        Id = r.Id,
                        RPCode = r.RPCode,
                        RPStatus = r.RPStatus,
                        StatusId = l==null?0:l.StatusId,
                        Photo = r.Photo
                    };
            return Model;
        }

        public IQueryable<Models.RequestVM> FindBy(System.Linq.Expressions.Expression<Func<Models.RequestVM, bool>> predicate)
        {
            List<RequestVM> all = new List<RequestVM>();
            return all.AsQueryable();
        }

        public void Add(Models.RequestVM r)
        {
            var Model =
                    new Request()
                    {
                        FamilyName = r.FamilyName,
                        Id = r.Id,
                        RPCode = r.RPCode,
                        RPStatus = r.RPStatus,
                        Photo=r.Photo
                    };
            var res = context.Requests.Add(Model);
            context.SaveChanges();
            
            var Link = new RequestsStatusesLink() {RequestId = res.Id, RPCode = res.RPCode, StatusId = r.StatusId};
            statusdb.RequestsStatusesLinks.Add(Link);
            statusdb.SaveChanges();
        }

        public void Delete(Models.RequestVM entity)
        {

        }

        public void DeleteById(int id)
        {
            var r = context.Requests.FirstOrDefault(s => s.Id == id);
            if (r == null) return;
            var l = statusdb.RequestsStatusesLinks.FirstOrDefault(u => u.RequestId == id && u.RPCode == r.RPCode);
            if (l != null) statusdb.RequestsStatusesLinks.Remove(l);
            context.Requests.Remove(r);
            context.SaveChanges();
            statusdb.SaveChanges();
        }

        public void Edit(Models.RequestVM r)
        {
            var model = context.Requests.FirstOrDefault(s => s.Id == r.Id && s.RPCode==r.RPCode);
            if (model == null) return;
            model.FamilyName = r.FamilyName;
            model.RPCode = r.RPCode;
            model.RPStatus = r.RPStatus;
            if (r.Photo!=null) model.Photo = r.Photo;
            context.SaveChanges();
            if (r.StatusId != 0)
            {
                var l = statusdb.RequestsStatusesLinks.FirstOrDefault(u => u.RequestId == r.Id && u.RPCode == r.RPCode);
                if (l == null)
                {
                    var Link = new RequestsStatusesLink() { RequestId = r.Id, RPCode = r.RPCode, StatusId = r.StatusId };
                    statusdb.RequestsStatusesLinks.Add(Link);
                }
                else
                {
                    l.StatusId = r.StatusId;  
                }
                statusdb.SaveChanges();
            }

        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void SetStatus(int[] ids)
        {
            var q = context.Requests.Where(t => ids.Contains(t.Id)).ToList();
            foreach (var r in q)
            {
                r.RPStatus = 3;
                var l = statusdb.RequestsStatusesLinks.FirstOrDefault(u => u.RequestId == r.Id && u.RPCode == r.RPCode);
                if (l == null)
                {
                    var Link = new RequestsStatusesLink() { RequestId = r.Id, RPCode = r.RPCode, StatusId = 3 };
                    statusdb.RequestsStatusesLinks.Add(Link);
                }
                else
                {
                    l.StatusId =3;
                }
            }
            statusdb.SaveChanges();
            context.SaveChanges(); 
        }
    }
}
