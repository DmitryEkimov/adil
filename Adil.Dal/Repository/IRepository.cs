﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Adil.Dal.Models;

namespace Adil.Dal.Repository
{
    public interface IRequestsRepository
    {

        IQueryable<RequestVM> GetAll();
        RequestVM GetSingle(int Id);
        IQueryable<RequestVM> FindBy(Expression<Func<RequestVM, bool>> predicate);
        void Add(RequestVM entity);
        void Delete(RequestVM entity);
        void DeleteById(int id);
        void Edit(RequestVM entity);
        void Save();
        void SetStatus(int[] ids);
        int GetStatus(int Id, int RPCode);
        string GetStatusName(int Id, int RPCode);
    }

    public interface IStatusesRepository
    {
        IQueryable<Status> GetAll();
        string CodeToStatus(int id);
        int StatusToCode(string status);
    }
}
