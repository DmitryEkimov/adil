﻿using Adil.Dal.Repository;
using Autofac;

namespace Adil.Dal
{
    public class DALModule : Module
    {
        public string ConnectionStringName { get; set; }
        public string SecondConnectionStringName { get; set; }
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            ConnectionStringName = "DefaultConnection";
            SecondConnectionStringName = "SecondConnection";
            builder.Register(c => new RequestsDbContext("Name="+ConnectionStringName))
                .As<RequestsDbContext>().InstancePerLifetimeScope();
            builder.Register(c => new StatusDbContext("Name=" + SecondConnectionStringName))
                .As<StatusDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<RequestsRepository>().As<IRequestsRepository>().InstancePerLifetimeScope();
            builder.RegisterType<StatusesRepository>().As<IStatusesRepository>().InstancePerLifetimeScope();
        }
    }
}
