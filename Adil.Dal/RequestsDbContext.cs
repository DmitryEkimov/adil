﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adil.Dal.Helpers;
using Adil.Dal.Models;
using System.Configuration;

namespace Adil.Dal
{
    public class RequestsDbContext: DbContext
    {
        public IDbSet<Request> Requests { get; set; } // acts
        public IDbSet<RequestsStatusesLink> RequestsStatusesLinks { get; set; } // addresses
        public IDbSet<Status> Statuses { get; set; } // authors
        
        static RequestsDbContext()
        {
            DbContextUtils<RequestsDbContext>.SetInitializer(null);
        }

        public RequestsDbContext()
            : base("Name=DefaultConnection")
        {
        }
        public RequestsDbContext(string connectionString)
            : base(connectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new RequestConfiguration());
            //modelBuilder.Configurations.Add(new StatusConfiguration());
            //modelBuilder.Configurations.Add(new RequestsStatusesLinkConfiguration());
        }
    }
}
