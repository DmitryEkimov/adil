﻿using Autofac;
using Autofac.Integration.Mvc;
using BootstrapMvcSample.Controllers;

namespace Adil
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterControllers(typeof(WebModule).Assembly);
            //.InjectActionInvoker();

            //builder.RegisterType<TransactedActionFilter>()
            //    .AsActionFilterFor<HomeController>()
            //    .InstancePerHttpRequest();

            //builder.RegisterAssemblyTypes(typeof(WebModule).Assembly)
            //    .AsClosedTypesOf(typeof(ITypeConverter<,>))
            //    .AsSelf();


            builder.RegisterModelBinders(typeof(WebModule).Assembly);

            builder.RegisterModelBinderProvider();

            builder.RegisterFilterProvider();

        }
    }
}