﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Adil.Models
{
    public class RequestVMInput
    {
        [DisplayName("Номер заявки")]
        public int Id { get; set; } //Номер заявки
        [Required]
        [DisplayName("Код РП")]
        public int RPCode { get; set; } // Код РП
        [Required]
        [DisplayName("Статус РП")]
        public int RPStatus { get; set; } // Статус РП
        [Required]
        //[MaxLength(256)]
        [DisplayName("ФИО")]
        public string FamilyName { get; set; } // ФИО
        [DisplayName("Фото")]
        [HttpPostedFileExtensions()]
        public HttpPostedFileBase Photo { get; set; }
        [Required]
        [DisplayName("Статус ЦУ")]
        public int StatusId { get; set; } // Статус ЦУ
    }
}