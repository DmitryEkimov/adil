﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Adil.Dal;
using Adil.Dal.Models;
using Adil.Dal.Repository;
using Adil.Helpers;
using Adil.Models;
using Datatables.Mvc;
using Models;
using Newtonsoft.Json;

namespace BootstrapMvcSample.Controllers
{
    public class RequestsController : BootstrapBaseController
    {
        private static List<HomeInputModel> _models = ModelIntializer.CreateHomeInputModels();
        private readonly IRequestsRepository repo;
        private readonly IStatusesRepository statuses;
        public RequestsController(IRequestsRepository _repo,IStatusesRepository _statuses) 
        {
            repo = _repo;
            statuses = _statuses;
        }
        [HttpGet]
        public ActionResult Index()
        {
            HttpCookie myCookie = this.Request.Cookies["Requests"];
            var model = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.Statuses = JsonConvert.SerializeObject(statuses.GetAll().Select(s => s.StatusName ).ToArray());
            model.Columns = new[] { "","№ заявки", "Статус РП", "Код РП", "ФИО", "Статус ЦУ","" };
            model.EntityName = "Requests";
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string[] col)
        {
            var ids = col.Select(int.Parse).ToArray();
            repo.SetStatus(ids);
            return RedirectToAction("Index");
        }

        public ActionResult GetDataTables(Datatables.Mvc.DataTable dataTable)
        {
            var query = repo.GetAll();

            var totalcount = query.Count();

            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                query = int.TryParse(dataTable.sSearch, out intval) ? query.Where(x => x.Id == intval) : query.Where(x => x.FamilyName.Contains(dataTable.sSearch));
            }

            if (!String.IsNullOrEmpty(dataTable.sSearchs[2]))
            {
                var searchterm = statuses.StatusToCode(dataTable.sSearchs[2]);
                if (searchterm != 0)
                query = query.Where(x => x.RPStatus == searchterm);
            }

            if (!String.IsNullOrEmpty(dataTable.sSearchs[5]))
            {
                var searchterm = statuses.StatusToCode(dataTable.sSearchs[5]);
                if (searchterm!=0)
                query = query.Where(x => x.StatusId == searchterm);
            }

            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                    case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                    case 2:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.RPStatus)
                                    : query.OrderByDescending(x => x.RPStatus);
                        break;
                    case 3:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.RPCode)
                                    : query.OrderByDescending(x => x.RPCode);
                        break;
                    case 4:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.FamilyName)
                                    : query.OrderByDescending(x => x.FamilyName);
                        break;
                    case 5:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.RPStatus)
                                    : query.OrderByDescending(x => x.RPStatus);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table =
                query.ToList()
                     .Select(
                         t =>
                         {
                             return new DataTableRow(t.Id.ToString(), "dtrowclass")
                                            {
                                                t.Id.ToString(),
                                                t.Id.ToString(),
                                                statuses.CodeToStatus(t.RPStatus),
                                                t.RPCode.ToString(),
                                                t.FamilyName,
                                                repo.GetStatusName(t.Id,t.RPCode),
                                                t.Id.ToString()
                                            };
                         }).ToList();
            HttpCookie myCookie = this.Request.Cookies["Requests"] ?? new HttpCookie("Requests");
            myCookie.Value = JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            this.Response.Cookies.Add(myCookie);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        [HttpPost]
        public ActionResult Create(RequestVMInput model)
        {
            if (ModelState.IsValid)
            {
                //model.Id = _models.Count==0?1:_models.Select(x => x.Id).Max() + 1;
                var photo=model.Photo != null ? model.Photo.InputStream.ToArray():null;
                var r = new RequestVM()
                        {
                            FamilyName = model.FamilyName,
                            Id = model.Id,
                            Photo = photo,
                            RPCode = model.RPCode,
                            RPStatus = model.RPStatus,
                            StatusId = model.StatusId
                        };
                repo.Add(r);
                Success("Заявка сохранена!");
                return RedirectToAction("Index");
            }
            Error("есть ошибки на форме.");
            return View(model);
        }

        public ActionResult Create()
        {
            return View(new RequestVM(){RPStatus = 1, StatusId = 1 });
        }

        public ActionResult Delete(int id)
        {
            //_models.Remove(_models.Get(id));
            repo.DeleteById(id);
            Information("заявка удалена!");
            return RedirectToAction("index");
        }
        public ActionResult Edit(int id)
        {
            var model = repo.GetSingle(id);
            if (model == null)
            {
                Error("Нет заявки с таким номером!");
                return RedirectToAction("Index");
            }
            
            return View("Create", model);
        }
        [HttpPost]        
        public ActionResult Edit(RequestVMInput model,int id)
        {
            if(ModelState.IsValid)
            {
                var photo=model.Photo != null ? model.Photo.InputStream.ToArray():null;
                var r = new RequestVM()
                        {
                            FamilyName = model.FamilyName,
                            Photo = photo,
                            RPCode = model.RPCode,
                            RPStatus = model.RPStatus,
                            StatusId = model.StatusId
                        };
                r.Id = id;
                repo.Edit(r);
                Success("Заявка обновлена!");
                return RedirectToAction("index");
            }
            return View("Create", model);
        }

		public ActionResult Details(int id)
        {
            var model = _models.Get(id);
		    if (model == null)
		    {
                Error("Нет заявки с таким номером!");
		        return RedirectToAction("Index");
            }
            return View(model);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
           
            //advise subsequent exception filters not to interfere and stop
            // asp.net from showing yellow screen of death
            filterContext.ExceptionHandled = true;

            //erase any output already generated
            filterContext.HttpContext.Response.Clear();
            Error(filterContext.Exception.Message);
            filterContext.Result = View(new RouteValueDictionary
             (new { area = "", controller = "Requests", action = "Index" }));
        }

    }
}
