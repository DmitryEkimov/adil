﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Adil.Dal.Repository;

namespace Adil.Controllers
{
    public class StatusesDropDownController : Controller
    {
        private readonly IStatusesRepository statuses;
        public StatusesDropDownController(IStatusesRepository _statuses)
        {
            statuses = _statuses;
        }

        public ActionResult GetDropdown(int selectedId, string name)
        {
            ViewData.Model = statuses.GetAll().ToList().Select(s=>new SelectListItem
                                 {
                                     Text = s.StatusName,
                                     Value = s.Id.ToString(),
                                     Selected = (s.Id == selectedId)
                                 });
            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(int),
                    name) { NullDisplayText = " выберите статус из списка" };
            return View("Dropdown");
        }

    }
}
